package cn.quantgroup.rabbitmq.hello;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * Created by mingzhu on 2016/12/10.
 */
public class Send {
    private static final String QUEUE_NAME = "hello";

    /**
     * 以下代码均为本机测试,RabbitMQ安装后默认启用guest账户,用户名guest,密码guest
     * 但是该账户默认仅能在localhost中使用.请注意.
     * 测试代码请根据自己的环境修改userName和password
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception{
        //创建连接工厂,设置连接地址，用户名，密码
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        factory.setUsername("admin");
        factory.setPassword("admin");
        //创建连接
        Connection connection = factory.newConnection();
        //通过连接创建信道,信道的创建和销毁代价比连接要小的多
        Channel channel = connection.createChannel();
        //声明队列,发送者和生产者都做了队列声明的操作
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        //发送的消息体
        String message = "hello rabbitmq";
        //发送消息,消息体格式为字节数组
        channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
        //关闭连接和通道
        channel.close();
        connection.close();
    }
}
